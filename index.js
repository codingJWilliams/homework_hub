const express = require('express');
const app = express();
var session = require('express-session');
var users = require("./usernames.json");
var bodyParser = require('body-parser');
var shell = require('shelljs');
const Filehound = require('filehound');
var dir = require("node-dir");
const path = require('path');
const fs = require("fs");
var jwt = require('jsonwebtoken');

var revisionHub = require("./routers/revisionhub.js")
app.use("/", revisionHub)

var dev = false;
var secret = "very secret little encryption key right here. Would be right upset if some nonce like warren got hold of it :P";

function getSchoolUsername(token) {
  var ppl = {
    "warren": "14ormew",
    "JayWilliams": "14williamsjm"
  };
  console.log(jwt.verify(token, secret));
  return ppl[jwt.verify(token, secret).username]
}

function getHomeworks(cb) {
  console.log("shitacos")
  dir.files("db", "dir", function (err, dirs) {
    console.log("tings")
    if (err) {
      console.log(err);
      socket.emit("error", err);
      return 1;
    } else {
      var ind = dirs.indexOf("db"+ path.sep +".git");
      if (ind > -1) {
        dirs.splice(ind, 1);
      }
      console.log(ind, dirs)
      var hwks = []
      for (var a = 0; a < dirs.length ; a++) {
        var jsonhwk = fs.readFileSync(dirs[a] + path.sep + "homework.json");
        hwks.push(JSON.parse(jsonhwk.toString()));
      }
      cb(hwks.sort(function (a,b) { // Sort homeworks by id. Fixes wierd bug where order changes on reload
        return parseInt(a.id) - parseInt(b.id)
      }))
    }

  }, {recursive: false})
}

app.use(session({secret: 'nvidnelnfjncljjnsklnflnlksnlfknfslknklsnfkllsfnlsknkfsnfklsnflkfniped;vuhgiugbvcirgufghiu oncubfchguibfug;hxiufhgxhfgxkuyxfgfjkgdjbzdgfEhnjgfhjbvfjfdhfdyhgygfhghgkghhy475yty8e848t68r77t9rhdyf98thviuchgdhgxiixygixugygduttxoxtfyuylxdftgiugfyfvhkufdygdhuthgtuyguighjgyigjkughghugfkdfhyguixilggzbffgjkfgzbkzgizgfdzugfugfbgbgfubg'}))
app.use(bodyParser.urlencoded());
function getNiceDate(epoch) {
  var d = new Date(epoch * 1000);
  var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  return d.getDate().toString() + " " + months[d.getMonth()] + " " + d.getFullYear().toString();
}
function ldb(epoch) {
  console.log(epoch)
  return epoch * 1000 < Date.now()
}
function guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return ("id_"+S4()+S4()+"_"+S4()+"_"+S4()+"_"+S4()+"_"+S4()+S4()+S4());
}
app.set('view engine', 'ejs');
app.use(express.static("assets"))
app.get('/', function (req, res) {
  getHomeworks(function (homeworks) {
    if (!req.session.hasOwnProperty("logged_in")) {
      req.session.logged_in = false;
      res.redirect("/login");
    } else if (req.session.logged_in) {
      res.render("pages/main", {active: 0, logged_in: true, username: req.session.username, homeworks: homeworks, getD: getNiceDate, hasPassed: ldb, guid: guidGenerator});
    } else {
      res.redirect("/login");
    }
  })
})
function getSubjects() {
  return new Promise(function (resolve, reject) {
    var subjects = JSON.parse(fs.readFileSync('subjects.json', 'utf8'));
    resolve(subjects)
  })
}

app.get("/jwn/:token", function (req, res) {
  var uname = getSchoolUsername(req.params.token);
  if (uname) {
    console.log(uname)
    req.session.logged_in = true;
    req.session.username = users[uname].name;
  }
  res.redirect("/")
})
app.post('/hook/', function (req, res) {
  var repository;
  res.send("ok");
  // Run external tool synchronously
  if (shell.exec('cd db && git pull').code !== 0) {
    shell.echo('Error: Git commit failed');
    shell.exit(1);
  }
})

app.get('/homework/:id', function (req, res) {
  getHomeworks(function (homeworks) {
    if (!req.session.hasOwnProperty("logged_in")) {
      req.session.logged_in = false;
      res.redirect("/login");
    } else if (req.session.logged_in) {
      var num = parseInt(req.params.id) - 1;
      res.render("pages/details", {active: 0, logged_in: true, username: req.session.username, homework: homeworks[num], getD: getNiceDate, hasPassed: ldb, guid: guidGenerator});
    } else {
      res.redirect("/login");
    }
  })
})
if (!dev) {
  app.get("/login", function (req, res) {res.render("pages/login", {active: 0, logged_in: false})})

} else {
  app.get("/login", function (req, res) {
    req.session.logged_in = true, req.session.username = "DevMode";
    res.redirect("/")
  })
}
app.post("/api/login_pls", function (req, res) {
  if (users[req.body.rdata.username].pin === req.body.rdata.pin) {
    req.session.logged_in = true;
    req.session.username = users[req.body.rdata.username].name;
    res.end("Logged In"); } else { req.session.logged_in = false; res.end("Logged Out")}

})
app.get("/logout", function (req, res) {
  req.session.logged_in=false;
  res.end("Logged out :-)")
})
app.listen(9999, function () {
  console.log('Example app listening on port 3000!')
})
