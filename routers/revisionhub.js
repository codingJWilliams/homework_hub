var express = require('express')
var app = express.Router()
var session = require('express-session');
var users = require("../usernames.json");
var bodyParser = require('body-parser');
var dir = require("node-dir");
const path = require('path');
const fs = require("fs");
var jwt = require('jsonwebtoken');
var session = require('express-session');
app.use(session({secret: 'nvidnelnfjncljjnsklnflnlksnlfknfslknklsnfkllsfnlsknkfsnfklsnflkfniped;vuhgiugbvcirgufghiu oncubfchguibfug;hxiufhgxhfgxkuyxfgfjkgdjbzdgfEhnjgfhjbvfjfdhfdyhgygfhghgkghhy475yty8e848t68r77t9rhdyf98thviuchgdhgxiixygixugygduttxoxtfyuylxdftgiugfyfvhkufdygdhuthgtuyguighjgyigjkughghugfkdfhyguixilggzbffgjkfgzbkzgizgfdzugfugfbgbgfubg'}))

function getSubjects() {
  return new Promise(function (resolve, reject) {
    var subjects = JSON.parse(fs.readFileSync('subjects.json', 'utf8'));
    resolve(subjects)
  })
}
function getNiceDate(epoch) {
  var d = new Date(epoch * 1000);
  var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  return d.getDate().toString() + "/" + d.getMonth().toString() + "/" + d.getFullYear().toString();
}
app.get('/rhub', function (req, res) {
  if (!req.session.hasOwnProperty("logged_in")) {  req.session.logged_in = false;
    res.redirect("/login");
  } else if (req.session.logged_in) {
    getSubjects().then(subjects => {
      res.render("RevisionHub/pages/home", {
        active: 1,
        logged_in: true,
        username: req.session.username,
        subjects: subjects
       });
    })
  } else { res.redirect("/login"); }
})
app.get('/rhub/subject/:sid', function (req, res) {
  if (!req.session.hasOwnProperty("logged_in")) {
    req.session.logged_in = false;
    res.redirect("/login");
  } else if (req.session.logged_in) {
    getSubjects().then(subjects => {
      res.render("RevisionHub/pages/subject", {
        active: 1,
        logged_in: true,
        username: req.session.username,
        subjects: subjects,
        subject: subjects.find( s => { return s.sid === req.params.sid })
       });
    })
  } else {
    res.redirect("/login");
  }
})
app.get('/rhub/subject/:sid/category/:cid', function (req, res) {
  if (!req.session.hasOwnProperty("logged_in")) {
    req.session.logged_in = false;
    res.redirect("/login");
  } else if (req.session.logged_in) {
    getSubjects().then(subjects => {
      res.render("RevisionHub/pages/category", {
        active: 1,
        logged_in: true,
        gnd: getNiceDate,
        username: req.session.username,
        subjects: subjects,
        subject: subjects.find( s => { return s.sid === req.params.sid }),
        category: subjects.find( s => { return s.sid === req.params.sid }).categories.find( c => { return c.cid === req.params.cid })
       });
    })
  } else {
    res.redirect("/login");
  }
})
app.get("/rhub/guide/:sid/:cid/:gid", function (req, res) {
  if (!req.session.hasOwnProperty("logged_in")) {
    req.session.logged_in = false;
    res.redirect("/login");
  } else if (req.session.logged_in) {
    getSubjects().then(subjects => {
      var subject = subjects.find( s => { return s.sid === req.params.sid })
      var category = subject.categories.find( c => { return c.cid === req.params.cid })
      var guide = category.guides[parseInt(req.params.gid)];
      console.log(subject, category, guide)
      res.render("RevisionHub/pages/guide", {
        active: 1,
        logged_in: true,
        gnd: getNiceDate,
        username: req.session.username,
        back: "/rhub/subject/" + req.params.sid + "/category/" + req.params.cid,
        guide: guide
       });
     })
  } else {
    res.redirect("/login");
  }
})

module.exports = app
